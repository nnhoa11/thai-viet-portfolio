import './App.css';
import { Routes, Route} from 'react-router-dom';
import HomePage from './components/HomePage';
import Navbar from './components/navbar';
import Works from './components/works';
import Virtual_reality_model from './components/projects/virtual-reality-model/virtual_reality_model';
import Automated_robot from './components/projects/automated-robot/automated_robot';
import Contact from './components/contact';
import Hutech_uniform1 from './components/projects/hutech-uniform1/hutechniform1';
import Hutech_uniform2 from './components/projects/hutech-uniform2/hutechuniform2';
import Mixigaming from './components/projects/mixi-gaming/mixigaming';
import {IoMdArrowUp} from 'react-icons/io'
import Olympus from './components/projects/olympus/olympus';
import Dragon_bridge from './components/projects/dragon-bridge/dragon_bridge';
import Golden_bridge from './components/projects/golden-bridge/golden_bridge';
import Milo from './components/projects/milo/milo';
import Gallery from './components/gallery';
// import Loading from './components/loading';
// import Footer from './components/footer';
// import MediaPrint from './components/mediaprint';
import About from './components/about';
import { useRef } from 'react';
import Footer from './components/footer';
import Loading from './components/loading';
function App() {
  // document.body.addEventListener("wheel", e=>{
  //   if(e.ctrlKey)
  //     e.preventDefault();//prevent zoom
  // });
  // function elementsOverlap(el1, el2) {
  //   const domRect1 = el1.getBoundingClientRect();
  //   const domRect2 = el2.getBoundingClientRect();
  
  //   return !(
  //     domRect1.top > domRect2.bottom ||
  //     domRect1.right < domRect2.left ||
  //     domRect1.bottom < domRect2.top ||
  //     domRect1.left > domRect2.right
  //   );
  // }
  const ref = useRef(null)
  const handleClick = () => {
      console.log('scroll')
      ref.current?.scrollIntoView({behaviour: 'smooth'})   
  }
  window.onload = () =>{
    document.getElementById('loading').style.display = 'none'
    document.getElementById('3d').classList.add('bold')
    document.getElementById("gal-vid1").style.width = document.getElementById("gal-vid1").clientHeight * (16 / 9) + "px"
    document.getElementById("gal-vid2").style.width = document.getElementById("gal-vid2").clientHeight * (16 / 9) + "px"
  }
  return (
    <div id="screen" className="App">
      <div id="App">
        <div ref={ref}></div>
        <Loading/>
        <Navbar></Navbar>
        <Routes>
          <Route exact path="/" element={<HomePage></HomePage>}></Route>
          {/* <Route path='/journey' element={<About></About>}></Route> */}
          <Route path='/about' element={<Contact></Contact>}></Route>
          <Route path='/works'element={<Works></Works>}></Route>
          <Route path='/works/virtual-reality-model' element={<Virtual_reality_model></Virtual_reality_model>}></Route>
          <Route path='/works/automated-robot' element={<Automated_robot></Automated_robot>}></Route>
          <Route path='/works/hutech-uniform1' element={<Hutech_uniform1></Hutech_uniform1>}></Route>
          <Route path='/works/hutech-uniform2' element={<Hutech_uniform2></Hutech_uniform2>}></Route>
          <Route path='/works/mixigaming' element={<Mixigaming></Mixigaming>}></Route>
          <Route path='/works/road-to-olympus' element={<Olympus></Olympus>}></Route>
          <Route path='/works/dragon-bridge' element={<Dragon_bridge></Dragon_bridge>}></Route>
          <Route path='/works/golden-bridge' element={<Golden_bridge></Golden_bridge>}></Route>
          <Route path='/works/milo' element={<Milo></Milo>}></Route>
          <Route path='/gallery' element={<Gallery></Gallery>}></Route>
        </Routes>
        <Footer></Footer>
        <div id="back-btn" className="btn-container">
            <button id="back-btn1" onClick={handleClick} className="btn-wrapper homepage-button"><IoMdArrowUp size='20px'/></button>
        </div>
        
      </div>
    </div>
  );
}

export default App;
