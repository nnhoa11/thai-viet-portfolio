import "../styles/Works.css"
import { Routes, Route } from "react-router-dom"
import Footer from "../components/footer"
import game_project1 from "../resources/game-project1.png"
import game_project2 from "../resources/game-project2.png"
import pod1 from "../resources/pod1.png"
import pod2 from "../resources/pod2.png"
import pod3 from "../resources/pod3.png"
import pod4 from "../resources/pod4.png"
import pod5 from "../resources/pod5.png"
import des3d1 from "../resources/3d1.png"
import des3d2 from "../resources/3d2.png"
import des3d3 from "../resources/3d3.png"
export default function Works(){
    window.onload = () =>{
        document.getElementById('loading').style.display = 'none'
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
    }
    return (
        <div className="works">  
            <div className="projects-container">
                <div className="projects-wrapper">
                    <div className='back-btn'>
                        <div id="back-pointer"></div>
                        <a href="/">Back to HomePage</a>
                    </div>
                    <div/>
                    <div/>
                    <div/>
                    <h1 className='title'>WORKS</h1>
                    <div/>
                    <h2>PROGRAMMING/ GAME DEVELOPING</h2>
                    <div/>
                    <a href="works/virtual-reality-model" className="project none-decoration black-span">
                        <img src={game_project1}></img>
                        <p>Virtual Reality Model Of Special National Monument Quang<br/> Tri Ancient Citadel (2022)</p>
                    </a>
                    <a href="/works/automated-robot" className="project none-decoration black-span">
                        <img src={game_project2}></img>
                        <p>Automated Robot Collects Samples For COVID-19 Testing<br/> (2021)</p>
                    </a>
                </div>
                <div className="projects-wrapper">
                    <h2>3D PROJECTS</h2>
                    <div/>
                    <a href="works/mixigaming" className="project none-decoration black-span">
                        <img src={des3d1}></img>
                        <p >THE ‘MIXIGAMING’ HYPER REALISTIC PORTRAIT</p>
                    </a>
                    <a href="works/dragon-bridge" className="project none-decoration black-span">
                        <img src={des3d2}></img>
                        <p >THE DRAGON BRIDGE</p>
                    </a>
                    <a href="works/golden-bridge" className="project none-decoration black-span">
                        <img src={des3d3}></img>
                        <p >THE GOLDEN BRIDGE</p>
                    </a>
                </div>
                <div className="projects-wrapper">
                    <h2>GRAPHIC DESIGN</h2>
                    <div/>
                    <a href="works/milo" className="project none-decoration black-span">
                        <img src={pod5}></img>
                        <p >REDESIGN PROJECT - WHAT’S YOUR MILO</p>
                    </a>
                    <a href="works/road-to-olympus" className="project none-decoration black-span">
                        <img src={pod4}></img>
                        <p >“ROAD TO OLYMPUS” Brand Identity</p>
                    </a>
                    <a href="works/hutech-uniform2" className="project none-decoration black-span">
                        <img src={pod2}></img>
                        <p >HUTECH Uniform Design Competition 2022</p>
                    </a>
                    <a href="works/hutech-uniform1" className="project none-decoration black-span">
                        <img src={pod1}></img>
                        <p >HUTECH Uniform Design Competition 2021</p>
                    </a>
                </div>
            </div>
           
            {/* <Footer /> */}
        </div>
    )
}