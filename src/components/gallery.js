import "../styles/gallery.css"
import logo from "../resources/logo.svg"
import { useState, useRef, useEffect } from "react"
import Graphic from "./gallery/graphic/graphic"
import Illustration from "./gallery/illustration/illustration"
import _3D from "./gallery/3d/3d"
import { Scrollbar } from "react-scrollbars-custom"
import {BsArrowLeft} from "react-icons/bs"
import logo_white from "../resources/logo-white.svg"
import Loading from "./loading"


export default function Gallery(){
    
    
    const [galleryState, changeState] = useState("3d");
   
   
    // const click3d = changeState('3d')
    const ref = useRef(null)
    let pos = 0
    let prevpos = 0
    let isdown= false
    let galleryPos = 0
    const handleClick = () => {
        const gallery = document.getElementById("gallery-container")
        const navbar = document.getElementById("navbar-container")
        const scrollthumb = document.getElementById("scroll-thumb")
        const backbtn = document.getElementById("back-btn2")
        const scrollbar = document.getElementById("scroll-bar")
        const app = document.getElementById("App")
        galleryPos = 0
        scrollthumb.style.width = "0px"
        gallery.style.transform = 'translateX('+ galleryPos + "px";
        backbtn.style.transform = 'translateX('+ (-1 * galleryPos) + "px";
        scrollbar.style.transform = 'translateX('+ galleryPos + "px";
        ref.current?.scrollIntoView({behavior: 'smooth'});
    }   
    useEffect(() =>{
        document.getElementById("back-btn").style.display = 'none'
        document.getElementById("navbar-links1").style.color = 'white'
        document.getElementById("navbar-links2").style.color = 'white'
        // document.getElementById("navbar-links3").style.color = 'white'
        document.getElementById("navbar-links4").style.color = 'white'
        document.getElementById("logo-svg").src = logo_white
        document.getElementById("App").style.maxWidth = "10000px"
        document.getElementById("screen").style.maxWidth = "10000px"
        // window.onload = () =>{
        //     document.getElementById('loading').style.display = 'none'
        // }
        document.getElementById('footer').style.display = 'none'
        const app = document.getElementById("App")
        const gallery = document.getElementById("gallery-container")
        const scrollbar = document.getElementById("scroll-bar")
        const scrollthumb = document.getElementById("scroll-thumb")
        const backbtn = document.getElementById("back-btn2")
        const content = document.getElementById("gallery-content")
        const text = document.getElementById("gallery-text")
        let scrollmouse = false
        scrollbar.addEventListener("click", (e) => {
            let ratio = (content.scrollWidth + text.scrollWidth - app.clientWidth) / scrollbar.offsetWidth
            scrollthumb.style.width = e.offsetX * -ratio + "px"
            galleryPos = e.offsetX * -ratio 
            gallery.style.transform = 'translateX('+ galleryPos +'px)';
            scrollbar.style.transform = 'translateX('+ (-1 * galleryPos) + "px)";
            backbtn.style.transform = 'translateX('+ (-1 * galleryPos) + "px)";
        })
        scrollbar.addEventListener("mousedown", (e) => {
            scrollmouse = true
            scrollthumb.style.width = e.offsetX + "px"
        });
        scrollbar.addEventListener("mousemove", (e) => {
            if (scrollmouse){
                isdown = false
                scrollthumb.style.width = e.offsetX + "px"
                let ratio = (content.scrollWidth + text.scrollWidth - app.clientWidth) / scrollbar.offsetWidth
                scrollthumb.style.width = e.offsetX * -ratio + "px"
                galleryPos = e.offsetX * -ratio 
                gallery.style.transform = 'translateX('+ galleryPos +'px)';
                scrollbar.style.transform = 'translateX('+ (-1 * galleryPos) + "px)";
                backbtn.style.transform = 'translateX('+ (-1 * galleryPos) + "px)";
            }
        });
        scrollbar.addEventListener("mouseup", (e) => {
            scrollmouse = false
        });
        scrollbar.addEventListener("mouseenter", (e) => {
            scrollmouse = false
        });
        scrollbar.addEventListener("mouseleave", (e) => {
            scrollmouse = false
        });
        gallery.addEventListener("mousedown", (e) =>{
            isdown = true
            prevpos = e.clientX || e.touches[0].clientX
            gallery.classList.add("grabbing")
        })
        gallery.addEventListener("mousemove", (e) =>{
            if (isdown && !scrollmouse){
                pos = e.clientX || e.touches[0].clientX
                galleryPos =  galleryPos + (pos -  prevpos) * 2.5
                prevpos = pos

                if (galleryPos > 0) galleryPos = 0
                if (Math.abs(galleryPos) >= gallery.scrollWidth - app.clientWidth){
                    galleryPos = -(gallery.scrollWidth - app.clientWidth)
                } 
                let ratio = (content.scrollWidth + text.scrollWidth - app.clientWidth) / scrollbar.offsetWidth
                scrollthumb.style.width = galleryPos / -ratio + "px"
                gallery.style.transform = 'translateX('+ galleryPos + "px";
                scrollbar.style.transform = 'translateX('+ (-1 * galleryPos) + "px";
                backbtn.style.transform = 'translateX('+ (-1 * galleryPos) + "px";
            }
        })
        gallery.addEventListener("wheel", (e) =>{
            galleryPos -= (galleryPos - e.deltaY <= 0 ? 1.5 * e.deltaY : 0);
            if (Math.abs(galleryPos) >= gallery.scrollWidth - app.clientWidth){
                galleryPos = -(gallery.scrollWidth - app.clientWidth)
            } 
            let ratio = (content.scrollWidth + text.scrollWidth - app.clientWidth) / scrollbar.offsetWidth
            scrollthumb.style.width = galleryPos / -ratio + "px"
            gallery.style.transform = 'translateX('+ galleryPos + "px";
            scrollbar.style.transform = 'translateX('+ (-1 * galleryPos) + "px";
            backbtn.style.transform = 'translateX('+ (-1 * galleryPos) + "px";
            
        })
        gallery.addEventListener("mouseenter", ()=>{
            isdown = false
        })
        gallery.addEventListener("mouseleave", (e) =>{
            isdown = false
        })
        gallery.addEventListener("mouseup", (e) =>{
            gallery.classList.remove("grabbing")
            isdown = false
        })
    })
    return(
    <div id="gallery-container"  className="gallery-container slide-back">
    
        <div id="gallery-text" className="gallery-text">
            <h1 className="mr0" ref={ref}>GALLERY</h1>
            <div>
                <p id="3d" onClick={(e) =>{
                    galleryPos = 0
                    document.getElementById(galleryState).classList.remove('bold')
                    changeState("3d")
                    e.target.classList.add('bold')
                    console.log(galleryState)
                    }} className="mr0">/3D ART</p>
                <p id ="graphic" onClick={(e) =>{
                    galleryPos = 0
                    document.getElementById(galleryState).classList.remove('bold')
                    changeState("graphic")
                    e.target.classList.add('bold')
                    console.log(galleryState)
                    }} className="mr0">/GRAPHIC DESIGN</p>
                <p id="illustration" onClick={(e) =>{
                    galleryPos = 0
                    document.getElementById(galleryState).classList.remove('bold')
                    changeState("illustration")
                    e.target.classList.add('bold')
                    console.log(galleryState)
                    }} className="mr-top0">/ILLUSTRATIONS</p>
            </div>
            <p>Welcome to GALLERY, a place where I experiment with different techniques in both 2D and 3D designs and explore my random thoughts and ideas, discovering the beauty within this magnificent world. You will notice that most of these ideas are related to Vietnam. I am influenced by the environment around me and my hometown. But let's explore further below. I don't limit myself, nor my capabilities.</p>
        </div>
        {galleryState === "3d" ? <_3D changeState={changeState} state={galleryState}/> : null}
        {galleryState === "graphic" ? <Graphic changeState={changeState} state={galleryState}/> : null}
        {galleryState === "illustration" ? <Illustration changeState={changeState} state={galleryState}/> : null}
        <div id="back-btn2" className="btn-container slide-back">
          <div></div>
          <button id="back-btn3" onClick={handleClick} className="slide-back btn-wrapper homepage-button"><BsArrowLeft/> Back to begin</button>
        </div>
        <div id="scroll-bar" className="scroll-bar-container slide-back">
            <div id="scroll-thumb" className="scroll-bar-thumb slide-back"></div>
        </div>
    </div>
    )
}