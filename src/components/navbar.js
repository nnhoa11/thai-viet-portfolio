import "../styles/Navbar.css"
import logo from "../resources/logo.svg"
// const logo = 
export default function Navbar() {
    return (
        <div id="navbar-container" className="navbar-container slide-back">
            <div className="logo-container">
                <img onClick={() =>{
                    window.location = "/"
                }} id="logo-svg" src={logo}></img>
            </div>
            
            <div id="navbar" className="navbar">
                <a id="navbar-links1" href='/works'>WORKS</a>
                <a id="navbar-links2" href='/gallery'>GALLERY</a>
                {/* <a id="navbar-links3" href='/journey'>JOURNEY</a> */}
                <a id="navbar-links4" href='/about'>ABOUT</a>
                {/* <input id="navbar-input" type="text" placeholder="Search"></input> */}
            </div>  
            
        </div>
    )
}