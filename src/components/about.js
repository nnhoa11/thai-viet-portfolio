import "../styles/About.css";
import journey from '../resources/Group 130.png'
import logo_white from "../resources/logo-white.svg"
import Loading from "./loading";
export default function About(){
window.onload = () => {
    document.getElementById('fl5').style.background = "#2A2A2A"
    document.getElementById('fl5').style.color = "white"
    document.getElementById("navbar-links1").style.color = 'white'
    document.getElementById("navbar-links2").style.color = 'white'
    document.getElementById("navbar-links3").style.color = 'white'
    document.getElementById("navbar-links4").style.color = 'white'
    document.getElementById("navbar-container").classList.add("navbar-journey-background")
    // document.getElementById("navbar-container").style.backgroundImage = "../resources/Group 130.png"
    document.getElementById("logo-svg").src = logo_white
    document.getElementById("App").style.backgroundImage = 'transparent'
    const navbar = document.getElementById("navbar-container")
    navbar.style.borderColor = "#888888"
    document.getElementById('loading').style.display = 'none'
}

    return (
    <div className="about">
 
        <div className='back-btn'>
            <div id="back-pointer"></div>
            <a href="/">Back to HomePage</a>
        </div>
        <img id="journey" src={journey}/>
        {/* <Footer/> */}
    </div>
)}
