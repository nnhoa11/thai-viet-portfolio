import "../styles/Footer.css"
import logo from "../resources/logo-white.png"
import facebook from '../resources/facebook.png'
import tiktok from '../resources/tiktok.png'
import behance from '../resources/behance.png'
import instagram from '../resources/instagram.png'
import figma from '../resources/figma-white.png'
import { useState } from "react"
export default function Footer(){
    // if (document.getElementById("footer").on)
    const [location, changeLocation] = useState("")
    return (
        <div id="footer" className="footer">
            <div className="footer-links">
                <div className="link-wrapper">
                    <a id="fl1" href="./">Homepage</a>
                    <a id="fl2" href="/about">About</a>
                    <a id="fl3" href="/works">Works</a>
                    <a id="fl4" href="/gallery">Gallery</a>
                </div>
                <p>Website made for Creativity Scholarship of RMIT.</p>
            </div>
            <div/>
            <div className="ld-footer">
                <img src={logo}/>
                <p className="mr0">All content © powercom2604@gmail.com</p>
            </div>
            <div className="cd-footer">
                <a target="_blank" href="https://www.facebook.com/cgthaiviet">
                    <img src={facebook}></img>
                </a>
                <a target="_blank" href="https://www.behance.net/cgthaiviet">
                    <img src={behance}></img>
                </a>
                <a target="_blank" href="https://www.tiktok.com/@cg.thaiviet?is_from_webapp=1&sender_device=pc">
                    <img src={tiktok}></img>
                </a>
                <a target="_blank" href="https://www.instagram.com/cgthaiviet">
                    <img src={instagram}></img>
                </a>
            </div>
            <div className="rd-footer">
                <h4 className="mr0">CONTACT</h4>
                <p className="mr0">powercom2604@gmail.com</p>
                <p className="mr0">+84 3892 86 144</p>
                <p className="mr0">Dong Ha city, Quang Tri province</p>
            </div>
        </div>
    )
}