import "../styles/HomePage.css"
import homepage_pic from '../resources/homepage-pic.png'
// import Footer from "./footer"
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useEffect, useState } from "react"
import Footer from "./footer"

export default function HomePage() {
    window.onload = () =>{
        document.getElementById('loading').style.display = 'none'
        document.getElementById("back-btn").style.display = "none"
        document.getElementById('fl1').style.background = "#2A2A2A"
        document.getElementById('fl1').style.color = "white"
        // document.querySelector('#App').addEventListener('wheel', preventScroll, {passive: false});
        // window.addEventListener('keydown', function(e) {
        //     if(e.keyCode == 32 && e.target == document.body) {
        //       e.preventDefault();
        //     }
        //   });
        // function preventScroll(e){
        //     e.preventDefault();
        //     e.stopPropagation();
        //     return false;
        // }
    }
    

    useEffect(()=>{
        var direction = "",
        oldx = 0,
        mousemovemethod = function (e) {
                
            if (e.pageX < oldx) {
                direction = "left"
            } else if (e.pageX > oldx) {
            direction = "right"
            }
            
            
            oldx = e.pageX;
                
        }
        const elArray = document.querySelectorAll(".thaiviet-hover")
        const app = document.getElementById("App")
        elArray.forEach(element => {
            element.addEventListener('mousemove', (e) =>{
                mousemovemethod(e)
                document.getElementById('thaivietpic').style.transform = (direction ==='left' ? 'translateX(-5px) scale(0.8)': 'translateX(5px) scale(0.8)')
            })
        });
    },[])
    return (
        <div id="home-page" className="home-page">

            <div className="front-text">
                    <h1 id="text1" className="mr-bot0 mr-top0"><span>HELLO! I’M THAI VIET Y HELLO! I’M THAI VIET Y</span></h1>   
                    <h2 id="text2"className="mr-top0 mr-bot0"><span>AKA CGTHAIVIET AKA CGTHAIVIET</span></h2> 
                    <h3 id="text3"className="mr-top0 mr-bot0"><span>3D ARTIST / GRAPHIC DESIGNER  / GAME DEVELOPER <br/>3D ARTIST / GRAPHIC DESIGNER  / GAME DEVELOPER</span><span>3D ARTIST / GRAPHIC DESIGNER  / GAME DEVELOPER</span></h3>        
                    <div className="front-text-divider">
                        <p grid-area="text" className="pd100">
                        Thai Viet Y is currently studying at Le Quy Don High School for the Gifted, located in Dong Ha city, Quang Tri province.<br/><br/>
                        He has over 3 years of experience in the field of Creative Design, utilizing 3D Art and Graphic Art to express his ideas, contribute value to the community, and strive for recognition. He has gained recognition through competitions such as ViSEF and SV-STARTUP, and has been featured in various online and print newspapers in Vietnam, including VNExpress, Lao Dong, Vietnamnet, Thanh Nien, CAND, Muc Tim magazine, and Hoa Hoc Tro magazine.<br/><br/>
                        Furthermore, he has established his personal brand through social media platforms such as TikTok and Facebook, where he has amassed over 25,000 followers and numerous videos that have reached millions of views. Additionally, he has had the opportunity to collaborate with various brands and influencers.
                        </p>
                        <div className="thaiviet-hover"/>
                        <div className="thaiviet-hover"/>
                        <div grid-area="links" className="links">
                            <div className="links-container">
                                <a href="/about">
                                    ABOUT ME
                                </a>
                                <div id="slash"/>
                            </div>
                            <div className="links-container">
                                <a href="/works">
                                    WORKS
                                </a>
                                <div id="slash"/>
                            </div>
                            <div className="links-container">
                                <a href="/gallery">
                                    GALLERY
                               </a>
                                {/* <div className="mr0 home" id="slash"/> */}
                            </div>
                           
                        </div>
                        <div className="thaiviet-hover"/>
                    </div>
                    <div className="background">
                        <div/>
                        <div id="colored-background-side">
                            <img id="thaivietpic" className="slide-back" grid-area="image"src={homepage_pic} />
                        </div>
                    </div>
                    
            </div>
            {/* <Footer/> */}
        </div>
    )
}