import ug from '../resources/unreal-engine.png'
import blender from '../resources/blender.png'
import zbrush from '../resources/zbrush.png'
import maya from '../resources/maya.png'
import figma from '../resources/figma.png'
import ai from '../resources/ai.png'
import ae from '../resources/ae.png'
import "../styles/HomePage.css"
import pr from '../resources/pr.png'
import pts from '../resources/pts.png'
import pt from '../resources/pt.png'
import laptop from '../resources/laptop-symbol.png'
import artist from '../resources/3d_artist_symbol.png'
import game from '../resources/game_symbol.png'
import cpp from '../resources/cpp.png'
import tiktok from '../resources/media-tiktok.png'
import behance from '../resources/media-behance.png'
import facebook from '../resources/media-facebook.png'
import press1 from '../resources/press1.png'
import press2 from '../resources/press2.png'
import press3 from '../resources/press3.png'
import press4 from '../resources/press4.png'
import press5 from '../resources/press5.png'
import press6 from '../resources/press6.png'
import press7 from '../resources/press7.png'
import press8 from '../resources/press8.png'
import press9 from '../resources/press9.png'
import press10 from '../resources/press10.png'
import press11 from '../resources/press11.png'
import press12 from '../resources/press12.png'
import press13 from '../resources/Group 146.png'
import press14 from '../resources/Group 147.png'
import press15 from '../resources/Group 148.png'
import Footer from './footer'
export default function Contact(){
    window.onload = () =>{
        document.getElementById('fl2').style.background = "#2A2A2A"
        document.getElementById('fl2').style.color = "white"
        document.getElementById('loading').style.display = 'none'
        
    }
    return (
        <div className="contact">
            <div className="about-me-container">
                <div className='back-btn'>
                    <div id="back-pointer"></div>
                    <a href="/">Back to HomePage</a>
                </div>
                <h1>ABOUT ME</h1>
                <div className="text-wrapper">
                <span id="first-span" >I‘m Thai Viet Y, born in 2005, and currently residing in Quang Tri, Vietnam. <br/>
“cgthaiviet” is my stage name on platforms such as Facebook, Behance, and TikTok since 2022 ("vietcailo" was my stage name from 2021 onward).<br/>
<br/>
I have accumulated over 3 years of experience in the field of Creative Design, specializing as a <span className="highlighted-span">3D Artist, Graphic Designer, and Game Developer.</span>. <br/>
<br/>
Ever since I was young, I have been deeply passionate about both Art and Technology. Throughout the years, I have aspired to merge and harmonize these two passions of mine
</span>
                    <span id="second-span">During my final years of secondary school, coinciding with the first COVID-19 outbreak, I found myself with more time to delve into research and explore different potential career paths. Among the various options I considered, I discovered my talent for <span className="highlighted">Graphic Design</span> and decided to pursue it throughout my high school years.<br/>
<br/>
In 11th grade, I took a leap and ventured into learning <span className="highlighted-span">3D Design</span>, and it didn't take long for me to develop a deep passion for it.<br/>
<br/>
When I reached 12th grade, I decided to combine my accumulated skills in 3D Design with my newfound knowledge of <span className="highlighted-span">Game Development</span>. With this fusion of expertise, I embarked on a personal project to create software that focused on recreating Vietnam's historical sites within a virtual environment. My aim was to contribute value to society through this endeavor.<br/>
<br/>
</span>
                </div>
            </div>
            <div className="roles-container">
                <h3>WHAT I DO?</h3>
                <div className="roles-wrapper">
                    <div className="role">
                        <img src={laptop}></img>
                        <h3 >GRAPHIC DESIGNER <br/>(2020  - present)</h3>
                    </div>
                    <div className="role">
                        <img src={artist}></img>
                        <h3 >3D ARTIST <br/>(2021 - present)</h3>
                    </div>
                    <div className="role">
                        <img src={game}></img>
                        <h3 >PROGRAMMER/<br/>GAME DEVELOPER <br/>(2022 - present)</h3>
                    </div>
                </div>
            </div>
            <div className='software-used'>
                <h3>SKILLS</h3>
                <div className="logos-container">
                    <div className="wrap-4-col">
                        <img src={ug}></img>
                        <img src={blender}></img>
                        <img src={zbrush}></img>
                        <img src={maya}></img>
                    </div>
                    <div className="wrap-3-col  ">
                        <img src={cpp}/>
                        <img src={figma}></img>
                        <div className="adobe-logos">
                            <img src={pts}/>
                            <img src={ai}/>
                            <img src={ae}/>
                            <img src={pr}/>
                            <img src={pt}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="highlight">
                <h3 className="black-span">HIGHLIGHT</h3>
                <div className="wrap-3-col">
                    <div>
                        <h3 className="blue-span mr-bot0">3rd Prize</h3>
                        <span className=" mr-top0 grey-span">ViSEF 2022</span>
                    </div>
                    <div>
                        <h3 className="blue-span mr-bot0">1st Prize</h3>
                        <span className=" mr-top0 grey-span">SV-STARTUP 2023</span>
                    </div>
                    <div>
                        <h3 className="blue-span mr-bot0">4th Prize</h3>
                        <span className=" mr-top0 grey-span">ViSEF 2023</span>
                    </div>
                </div>
                <div className="wrap-4-col">
                    <div>
                        <h3 className="blue-span mr-bot0">70.00$ Offer</h3>
                        <span className=" mr-top0 grey-span">Fulbright University </span>
                    </div>
                    <div>
                        <h3 className="blue-span mr-bot0">3 Design Certificates</h3>
                        <span className=" mr-top0 grey-span">Adobe Certified Professional</span>
                    </div>
                    <div>
                        <h3 className="blue-span mr-bot0">4th Prize</h3>
                        <span className=" mr-top0 grey-span">HUTECH Uniform Design<br/> Competition 2021</span>
                    </div>
                    <div>
                        <h3 className="blue-span mr-bot0">25.000+</h3>
                        <span className=" mr-top0 grey-span">Followers on Tiktok<br/> 2D/ 3D Design/ VR Game Dev<br/> content</span>
                    </div>
                </div>
            </div>
            <div className="work-script">
                <div></div>
                <h3 className="bold-span black-span">WORK/PROJECT</h3>
                <h3 className="bold-span black-span">ACHIEVEMENT/CERTIFICATE</h3>
                <div>
                    <h3 className="role bold-span ">PROGRAMMER/<br/>GAME DEVELOPER <br/>(2022 - present)</h3>            
                    <img src={game}></img>
                </div>
                <h4 className="black-span">
                Virtual Reality Model Of Special National Monument Quang Tri Ancient Citadel <span className="grey-span">(08/2022 - 03/2023)</span><br/><br/>
Automated Robot Collects Samples For COVID-19 Testing<br/> <span className="grey-span">(09/2021 - 03/2022)</span><br/><br/>
Content creator on Tiktok
                </h4>
                <h4 className="black-span">1st Prize at SV-STARTUP 2023 National Level <span className="grey-span">(03/2023)</span><br/><br/>
4th Prize at Vietnam Science and Engineering Fair 2023 National Level <span className="grey-span">(03/2023)</span><br/><br/>
2nd Prize at Vietnam Science and Engineering Fair 2023 Provincial Level <span className="grey-span">(01/2023)</span><br/><br/>
Adobe Certified Professional certificate 
(Graphic Design & Illustration using Adobe Illustrator) <span className="grey-span">(05/2022)</span><br/><br/>
Adobe Certified Professional certificate <br/>
(Visual Design using Adobe Photoshop) <span className="grey-span">(05/2022)</span><br/><br/></h4>
               
                
                <div>
                    <h3 className="bold-span role">3D ARTIST <br/>(2021 - present)</h3>
                    <img src={artist}></img>
               </div>
                <h4 className="black-span">
                Freelancer<br/><br/>
                Short-term Designer at Mixer <span className="grey-span">(11/2022)</span><br/><br/>
                Chairman of Le Quy Don Graphic Design Club <span className="grey-span">(02/2022)</span>
                </h4>
                <h4 className="black-span">
                3rd Prize at Vietnam Science and Engineering Fair 2022
National Level (03/2022) <br/><br/>
1st Prize at Vietnam Science and Engineering Fair 2022 Provincial Level <span className="grey-span">(01/2022)</span><br/><br/>
1st Prize at 3D Artwork Creative minigame by 3DF <span className="grey-span">(11/2021)</span><br/><br/>
Adobe Certified Professional certificate <br/>
(Visual Design Using Photoshop) <span className="grey-span">(11/2021)</span>
                </h4>
                <div>
                    <h3 className="role bold-span ">GRAPHIC DESIGNER <br/>(2020  - present)</h3>
                    <img src={laptop}></img>
                </div>
                <h4 className="black-span">
                Freelancer<br/><br/>
Designer/Influencer at “Den Truong Cho Em” Raise Funds Project <span className="grey-span">(01/2023)</span><br/><br/>
Designer at Le Quy Don High School Startup Club <span className="grey-span">(12/2022)</span><br/><br/>
UI Designer at Bambloo <span className="grey-span">(10/2022)</span><br/><br/>
Designer at “Road To Olympus” <span className="grey-span">(03/2022)</span><br/><br/>
Design Mentor at 3F Challenge <span className="grey-span">(06/2021)</span><br/><br/>
Designer at SKITUS Education <span className="grey-span">(05/2021 - 09/2021)</span>
                </h4>
                <h4 className="black-span">
                    4th Prize at CiC - Creative Idea Challenge Finalist 2022<br/><br/>
    Top 20 at HUTECH Uniform Design Competition 2022<br/><br/>
    2nd Prize at  ”Cuộc thi truyền thông - đẩy lùi COVID 2021” by LQD Press Club - Media<br/><br/>
    4th Prize at HUTECH Uniform Design Competition Finalist 2021
                </h4>
                
            </div>
            <div className='social-media'>
                <h3 className="black-span">SOCIAL MEDIA</h3>
                <div className='platform-container'>
                    <a target="_blank" href='https://www.tiktok.com/@cg.thaiviet?is_from_webapp=1&sender_device=pc' className='platform-wrapper'>
                        <img className="sd_pic" src={tiktok}></img>
                        <p className='bold-span'>Tiktok</p>
                    </a>
                    <a target="_blank" href='https://www.behance.net/cgthaiviet' className='platform-wrapper'>
                        <img className="sd_pic" src={behance}></img>
                        <p className='bold-span'>Behance</p>
                    </a>
                    <a target="_blank"  href='https://www.facebook.com/cg.thaiviet' className='platform-wrapper'>
                        <img className="sd_pic" src={facebook}></img>
                        <p className='bold-span'>Facebook</p>
                    </a>
                </div>
            </div>
            <div className='newspaper'>
                <h3 className="black-span">PRESS</h3>
                <div className='newspaper-container'>
                    <a target='_blank' href='https://laodong-vn.translate.goog/xa-hoi/hoc-sinh-lop-11-o-quang-tri-che-tao-robot-lay-mau-xet-nghiem-covid-19-1013610.ldo?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press1}/>
                    </a>
                    <a target='_blank' href='https://phunutoday-vn.translate.goog/nam-sinh-quang-tri-che-tao-thanh-cong-robot-lay-mau-xet-nghiem-covid-19-d314620.html?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press2}/>
                    </a>
                    <a target='_blank' href='https://kenhtuyensinh-vn.translate.goog/hai-nam-sinh-quang-tri-che-tao-thanh-cong-robot-ho-tro-lay-mau-xet-nghiem-covid-19?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press3}/>
                    </a>
                    <a target='_blank' href='https://vietnamnet-vn.translate.goog/robot-lay-mau-xet-nghiem-covid-gia-20-trieu-dong-cua-hoc-sinh-quang-tri-815409.html?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press4}/>
                    </a>
                    <a target='_blank' href='https://www.youtube.com/watch?v=97ZaHPiBd_8'> 
                        <img src={press5}/>
                    </a>
                    <a target='_blank' href='https://cadn-com-vn.translate.goog/print/tu-hao-hoc-sinh-truong-thpt-chuyen-le-quy-don-quang-tri-post275245.html?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press6}/>
                    </a>
                    <a target='_blank' href='https://vnexpress-net.translate.goog/hoc-sinh-tai-hien-thanh-co-quang-tri-tren-thuc-te-ao-4586392.html?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press7}/>
                    </a>
                    <a target='_blank' href='https://thanhnien-vn.translate.goog/dua-thanh-co-quang-tri-vao-mo-hinh-ao-3d-185230401224244897.htm?_x_tr_sl=vi&_x_tr_tl=en&_x_tr_hl=vi&_x_tr_pto=wapp'> 
                        <img src={press8}/>
                    </a>
                    <a target='_blank' href='https://thanhnien-vn.translate.goog/dua-thanh-co-quang-tri-vao-mo-hinh-ao-3d-185230401224244897.htm?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press9}/>
                    </a>
                    <a target='_blank' href='https://www.youtube.com/watch?v=CWBKkS2k8D8&'> 
                        <img src={press10}/>    
                    </a>
                    <a target='_blank' href='https://nhandantv-vn.translate.goog/tai-hien-di-tich-lich-su-bang-mo-hinh-ao-d223869.htm?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press11}/>
                    </a>
                    <a target='_blank' href='https://nhandan-vn.translate.goog/gop-phan-giao-duc-va-quang-ba-du-lich-qua-mo-hinh-ao-thanh-co-quang-tri-post744785.html?_x_tr_sl=vi&_x_tr_tl=en'> 
                        <img src={press12}/>
                    </a>
                    <a target='_blank' href='https://www.facebook.com/cgthaiviet/posts/pfbid03SGsfJyQD1q9pEdQLVdubKDD94tdbWZpmP74iVewi98yP5MSHBW1qx2fNbJqkQDkl'> 
                        <img src={press13}/>
                    </a>
                    <a target='_blank' href='https://muctim-tuoitre-vn.translate.goog/nhung-thiet-ke-cua-viet-y-69568.htm?_x_tr_sl=vi&_x_tr_tl=en&_x_tr_hl=vi&_x_tr_pto=wapp'> 
                        <img src={press14}/>
                    </a>
                    <a target='_blank' href='https://www.facebook.com/vtv24ViecTuTe/photos/a.999750263375419/5716600668356998'> 
                        <img src={press15}/>
                    </a>
                </div>
            </div>
        </div>
    )
}