import "../styles/Loading.css";
export default function Loading(){
    return(
        <div id="loading" className="loading">
            <div class="container">
                <div class="spinner"></div>
            </div>
        </div>
    )
}