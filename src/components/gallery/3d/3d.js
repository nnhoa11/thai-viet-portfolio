import pic1 from "./1.png"
import pic3 from "./3.png"
import pic4 from "./4.png"
import pic5 from "./5.png"
import pic6 from "./6.png"
import pic7 from "./7.png"
import pic8 from "./8.png"
import pic9 from "./9.png"
import pic10 from "./10.png"
import pic11 from "./11.png"
import pic12 from "./12.png"
import pic13 from "./13.png"
import pic14 from "./14.png"
import pic15 from "./15.png"
import pic16 from "./16.png"
import pic17 from "./17.png"
import pic18 from "./18.png"
import pic19 from "./19.png"

import ReactPlayer from "react-player/lazy"

export default function _3D(props){
    return(
        <div id="gallery-content" className="_3d gallery-pics">
                <img src={pic1}/>
                <img src={pic3}/>
                <img src={pic4}/>            
                <ReactPlayer controls="true" url="https://www.youtube.com/watch?v=WE3jcS9bIGs&ab_channel=ThaiVietCG" height="100%" id="gal-vid1"></ReactPlayer>
                <img src={pic5}/>
                <img src={pic6}/>
                <img src={pic7}/>
                <img src={pic8}/>
                <img src={pic9}/>
                <img src={pic10}/>
                <img src={pic11}/>
                <img src={pic12}/>
                <img src={pic13}/>
                <img src={pic14}/>
                <img src={pic15}/>
                <img src={pic16}/>
                <ReactPlayer controls="true" url="https://www.youtube.com/watch?v=7mxoMILzPiM&ab_channel=ThaiVietCG" height="100%" id="gal-vid2"></ReactPlayer>
                <img src={pic17}/>
                <img src={pic18}/>
                <img src={pic19}/>
                <div className="gallery-text ending-text">
                    <div>
                        <h3 onClick={(e) =>{
                            const gallery = document.getElementById("gallery-container")
                            const scrollbar = document.getElementById("scroll-bar")
                            const backbtn = document.getElementById("back-btn2")
                            const scrollthumb = document.getElementById("scroll-thumb")
                            scrollthumb.style.width = "0px"
                            gallery.style.transform = 'translateX(0)';
                            scrollbar.style.transform = 'translateX(0px)';
                            backbtn.style.transform = 'translateX(0)';
                            setTimeout(props.changeState("3d"), 800)
                            }} className="mr0">/3D ART</h3>
                        <h3 onClick={(e) =>{
                            const gallery = document.getElementById("gallery-container")
                            const scrollbar = document.getElementById("scroll-bar")
                            const backbtn = document.getElementById("back-btn2") 
                            const scrollthumb = document.getElementById("scroll-thumb")
                            scrollthumb.style.width = "0px"
                            gallery.style.transform = 'translateX(0)';
                            scrollbar.style.transform = 'translateX(0px)';
                            backbtn.style.transform = 'translateX(0)';
                            setTimeout(props.changeState("graphic"), 800)
                            }} className="mr0">/GRAPHIC DESIGN</h3>
                        <h3 onClick={(e) =>{
                            const gallery = document.getElementById("gallery-container")
                            const scrollbar = document.getElementById("scroll-bar")
                            const backbtn = document.getElementById("back-btn2") 
                            const scrollthumb = document.getElementById("scroll-thumb")
                            scrollthumb.style.width = "0px"
                            gallery.style.transform = 'translateX(0)';
                            scrollbar.style.transform = 'translateX(0px)';
                            backbtn.style.transform = 'translateX(0)';
                            setTimeout(props.changeState("illustration"), 800)
                            }} className="mr-top0">/ILLUSTRATIONS</h3>
                    </div>
                    <p>Welcome to my visual lab, a secret site where I experiment with different things in graphics and my random thoughts-ideas and discover what is possible in this beautiful world. You may find most of these are Vietnam-relating concepts. I get influenced by my surroundings and beloved hometown. But explore more below. I don't limit myself, as well as my capability.</p>
                </div>
        </div>
    )
}