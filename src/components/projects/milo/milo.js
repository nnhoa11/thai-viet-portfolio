import "./milo.css"
import Footer from "../../footer"
import pic1 from "./recources/what's-your-sd 1.png"
import pic2 from "./recources/POSTER- 1.png"
import pic3 from "./recources/FB 1.png"
import pic4 from "./recources/MOCKUP 1.png"
import pic5 from "./recources/Billboard-mockup 1.png"
import pic6 from "./recources/Billboard 1.png"
import pic7 from "./recources/Group 115.png"
export default function Milo() {
    window.onload = () =>{
      
        document.getElementById('loading').style.display = 'none'
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
    }
    return (
        <div className="milo">
            <div className='back-btn'>
                <div id="back-pointer"></div>
                <a href="/works">Back to Works</a>
            </div>
            <div className='project-title'>
                <h3 className="mr0">PROJECT</h3>
                <h1 className="mr0">REDESIGN PROJECT - WHAT’S YOUR MILO</h1>
            </div>
            <div className="project-member-6">
                <div id="only" className="member-container">
                    <h4 className="mr-bot0">Concept, Script, Design</h4>
                    <h4 className="mr-top0">Thai Viet Y - Phan Ngan Ha</h4>
                </div>
            </div>
            <div className="grid-2-col">
                <div className="milo-text">
                    <div>
                        <img src={pic1}/>
                        <p>The image of athletes playing sports is no longer attract the attention of young consumers. In this digital era, young adults are prone to find more colourful, innovative designs for their products. However, the shape surrounding the Milo logo will be kept as its meaning - the finishing line representing victory and achievement - was kept with slight modification to keep the symbolic and familiar recognition for the Milo packaging. The demand for self-expressing among young adults is the most influencing factor that drives their purchasing decisions. Therefore, by using 24 alphabets in 24 packaging models, teens will have the urge to own a can printing the initial alphabet in their names. On the surrounding, there will be three themes related to teens' popular interests namely sports, music and technology. Another special feature is the application of augmented reality (AR). Teenagers can scan the product by an AR app, type in their name and voila, it will pop up with theme elements surrounding it. This will be attractive to the youngster since they will have an engaging interaction with the product and also share their personal identities.</p>
                    </div>
                    <div>
                        <h2 className="mr-bot0">HOW IT WORKS?</h2>
                        <p className="mr0">The purpose is to create a platform for young adults to share their personal branding with social online platforms with the hashtag #thatsmyMilo when they scan the can with their phone. The product can also be gifted or sent virtually to crush, friends and family to have a spreading impact on their surroundings. Therefore, besides the nutritional purpose that Milo milk provides, the packaging will have a collective value for consumers so that they will not throw the empty can away. As a result, the design will have a marketing as well as an environmentally friendly impact. To promote the product, Milo can cooperate with teen influencers and celebrities and launch offline marketing campaigns at schools, buses or railways. Eventually, every youngster will own at least one can that is personalized specifically to them.</p>
                    </div>
                </div>
                <img id="milo-expand" src={pic2}/>
                <img src={pic3}/>
                <img src={pic4}/>
            </div>
            <div className="milo-pics">
                <h2 className="mr-bot0">OOH</h2>
                <img src={pic5}/>
                <h2>DESIGN</h2>
                <img src={pic6}/>
                <img src={pic7}/>
            </div>
            {/* <Footer/> */}
        </div>
    )
}