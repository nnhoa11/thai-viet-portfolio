import Footer from "../../footer"
import "./mixigaming.css"
import ReactPlayer from "react-player/lazy"
import pic1 from "./recources/pic1.png"
import pic2 from "./recources/pic2.png"
import pic3 from "./recources/pic3.png"
import pic4 from "./recources/pic4.png"
import pic5 from "./recources/pic5.png"
import pic6 from "./recources/pic6.png"
import pic7 from "./recources/pic7.png"
import pic8 from "./recources/pic8.png"
import pic9 from "./recources/pic9.png"
import pic10 from "./recources/pic10.png"
import pic11 from "./recources/pic11.png"
import pic12 from "./recources/pic12.png"
import pic13 from "./recources/pic13.png"
import pic14 from "./recources/pic14.png"
import pic15 from "./recources/pic15.png"
import pic16 from "./recources/pic16.png"
import pic18 from "./recources/pic18.png"
import pic19 from "./recources/pic19.png"
import logo_white from "../../../resources/logo-white.svg"
import { useEffect } from "react"
export default function Mixigaming(){
    
    // window.onunload = () =>{
        
    // }
    window.onload =() => {
        document.getElementById("navbar-links2").style.color = 'white'
        document.getElementById("navbar-links1").style.color = 'white'
        document.getElementById("navbar-links3").style.color = 'white'
        document.getElementById("navbar-links4").style.color = 'white'
        document.getElementById("logo-svg").src = logo_white
        document.getElementById("App").style.background = "black"
        // document.getElementById("navbar-input").style.borderColor = "#888888"
        const navbar = document.getElementById("navbar-container")
        navbar.style.borderColor = "#888888"
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
        document.getElementById('loading').style.display = 'none'
    }
    // useEffect(() => {
        
    // })
    return (
    <div id="mixi" className="mixigaming">
        <div className='back-btn'>
            <div id="back-pointer"></div>
            <a href="/works">Back to Works</a>
        </div>
        <div className='project-title'>
            <h3 className="mr0">PROJECT</h3>
            <h1 className="mr0">THE ‘MIXIGAMING’ HYPER REALISTIC PORTRAIT</h1>
        </div>
        <div className="img-container expand-fullscreen">
            <img src={pic1}/>
            <div className="grid-3-col">
                <div>
                    <h4>
                    SCULPT - Zbrush<br/>
                    RETOPOLOGY - Blender<br/>
                    MULTIRES - Blender
                    </h4>
                    <p>
                    Using Multires Modifier and Pores texture Aplha to reduce huge amount of vertices.
                    </p>
                </div>
                <img src={pic2}/>
                <div className="flex-2-row">
                    <p>Pores alpha map :</p>
                    <img src={pic3}/>
                </div>
                <div>
                    <h4>UV UNWRAP</h4>
                    <p>Total 142.624 faces</p>
                </div>
                <img src={pic4}/>
                <img src={pic5}/>
                <div>
                    <h4>SSS HANDPAINTING SKIN</h4>
                    <p>Photoshop + Blender</p>
                </div>
                <img src={pic6}/>
                <img src={pic7}/>
                <h4>ROUGHNESS, SUBDIVISION MAP</h4>        
                <img src={pic8}/>
                <img src={pic9}/>
                <div>
                    <h4>HAIR PACTICLES</h4>
                    <p>Blender</p>
                </div>
                <img src={pic10}/>
                <ReactPlayer controls="true" url="https://youtube.com/shorts/JQzCCdujTlg?feature=share" height="476px" width="476px"/>
                
            </div>
            <div className="flex-col">
                <h3>PROCEDURAL MATERIAL NODES</h3>
                <p>Skin material</p>
                <img src={pic11}/>
                <img src={pic12}/>
                <div>
                    <p>Hair material</p>
                    <img src={pic13}/>
                </div>
                <div>
                    <p>Eye material</p>
                    <img src={pic15}/>
                </div>
                <img src={pic19}/>
                <h2 className="center-img">THANKS FOR WATCHING!</h2>
            </div>
        </div>
        {/* <Footer/> */}
    </div>
    )
}