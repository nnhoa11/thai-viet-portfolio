import "./hutechuniform2.css"
import Footer from "../../footer"
import pic1 from "./recources/pic1.png"
import pic2 from "./recources/pic2.png"
import pic3 from "./recources/pic3.png"
import pic4 from "./recources/pic4.png"
import pic5 from "./recources/pic5.png"
import pic6 from "./recources/pic6.png"
import pic7 from "./recources/pic7.png"

export default function Hutech_uniform2(){
    window.onload = () =>{
                document.getElementById('loading').style.display = 'none'
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
    }
    return (
        <div className="hutech-uniform2">
           <div className='back-btn'>
                <div id="back-pointer"></div>
             <a href="/works">Back to Works</a>
            </div>
            <div className='project-title'>
                <h3 className="mr0">PROJECT</h3>
                <h1 className="mr0">HUTECH Uniform Design Competition 2022</h1>
            </div>
            <div className="project-member-6 expand-fullscreen">
                <div  className="member-container">
                    <h4 className="mr-bot0">Director</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Designer</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Script</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Concept</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Photographer</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Compositing</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
            </div>
            <div className="grid-2-col">
                <h2 className="mr-bot0">ALBUM TOPIC</h2>
                <div/>
                <div >
                    <p>Afar in distance but not in heart. The school years were already fast-paced, but the COVID-19 made it feel like a fleeting gust of wind. </p>
                    <p>Online learning has resulted in fewer memories and moments shared together, but it hasn't diminished our sense of unity. On the contrary, we understand each other's difficulties, share and sympathize with one another. Even though we are only separated by a screen, emotions still blossom within us.</p>
                    <p>The earlier challenges and pressures arrive, the more time we have to prepare and mature. A class community will continue on their chosen path, even if each individual goes in a different future, our hearts are always pointing towards each other.</p>
                </div>
                <img src={pic1}/>
                <h2 className="mr-bot0">SUBJECTS</h2>
                <div/>
                <div>
                    <span>Information Technology - Coding - Routine - COVID-19 Pandemic.</span><br/>
                    <span>Color : Ocean Blue and Dark Grey.</span><br/>
                    <span>Symbol : Le Quy Don high school’s logo - Binary code.</span><br/>
                    <h3 className="mr-top0 mr-bot0">Pantone:</h3>
                </div>
                <img src={pic2}/>
                <h2>DESIGN</h2>
                <div/>
                <div>
                    <p>IT is a hot industry in recent times, especially in the 4.0 Era and the Automation Industry. Someone said "IT is the king of professions", "Sit around and earn a few thousand dollars a month"... But few people know that in order to achieve the above, the "Coder" must work at full capacity. Dedication to brainpower non-stop. The "night owls" are used to sitting code from night to morning, familiar with the dark theme. So I used the familiar gray font as the main color for this uniform design. True to our class motto "As long as we live, we code", we transformed it into a programming language on the back of our shirts. The colors on the code are used to distinguish the type of variables, functions, things that are too familiar. The back side is annotated with 2 green lines that mean “This is not a bug, this is a feature!” – is a familiar slogan of IT people when this error inadvertently becomes a unique feature of the program. It also implies: “No one is perfect, my weakness is unintentional. How about being your own unique? Don't be too perfectionist, just be yourself!"</p>
                    <p>The front of the shirt is the Logo of our school - Le Quy Don High School for the Gifted with 11 IT class name. Below is an additional binary code, when translated it mean "11 IT". That's one way to distinguish the IT class from the others.</p>
                </div>
                <img src={pic3}></img>
                <img src={pic4} className="expand-img"/>
                <div className="white-span expand-fullscreen img-background">
                    <h2>RESULT</h2>
                    <div className="center-imgs">
                        <img src={pic5}/>
                        <img src={pic6}/>
                        <img src={pic7}/>
                    </div>
                </div>
                <div className="expand500"/>
            </div>
            {/* <Footer/> */}
        </div>
    )
}