import "./virtual_reality_model.css"
import ReactPlayer from "react-player/lazy"
import Footer from "../../footer"
import pic1 from "./recources/pic1.png"
import pic2 from "./recources/pic2.png"
import pic3 from "./recources/pic3.png"
import pic4 from "./recources/pic4.png"
import pic5 from "./recources/pic5.png"
import pic6 from "./recources/pic6.png"
import pic7 from "./recources/pic7.png"
import pic8 from "./recources/pic8.png"
import pic9 from "./recources/pic9.png"
import pic10 from "./recources/pic10.png"
import pic11 from "./recources/pic11.png"
import pic12 from "./recources/pic12.png"
import pic15 from "./recources/pic15.png"
import pic16 from "./recources/pic16.png"
import pic17 from "./recources/pic17.png"
import pic18 from "./recources/pic18.png"
import pic32 from "./recources/pic32.png"
import pic33 from "./recources/pic33.png"
import pic34 from "./recources/pic34.png"
import pic35 from "./recources/pic35.png"
import pic36 from "./recources/pic36.png"
import pic37 from "./recources/pic37.png"
import pic38 from "./recources/pic38.png"
import pic39 from "./recources/pic39.png"
import pic40 from "./recources/pic40.png"
import pic41 from "./recources/pic41.png"
import pic42 from "./recources/pic42.png"
import pic43 from "./recources/pic43.png"
export default function Virtual_reality_model(){
    window.onload = () =>{
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
        document.getElementById("demo-vid1").style.height = document.getElementById("demo-vid1").clientWidth * 0.5625 + "px"
        document.getElementById("demo-vid2").style.height = document.getElementById("demo-vid2").clientWidth * 0.5625 + "px"
        document.getElementById('loading').style.display = 'none'
    
    }
    return (
    <div className="virtual_reality_model">
        <div className='back-btn'>
            <div id="back-pointer"></div>
            <a href="/works">Back to Works</a>
        </div>
        <div className="project-title">
            <h3>PROJECT</h3>
            <h1>Virtual Reality Model Of Special National Monument Quang Tri Ancient Citadel (2022)</h1>
        </div>
        <div className="project-member-5 expand-fullscreen">
            <div className="member-container">
                <p className="mr-bot0">Leader</p> 
                <p className="mr-top0">Thai Viet Y</p>
            </div>
            <div className="member-container">
                <p className="mr-bot0">Designer</p>
                <p className="mr-top0">Thai Viet Y</p>
            </div>
            <div className="member-container">
                <p className="mr-bot0">Programming</p>
                <p className="mr-top0">Thai Viet Y</p>
            </div>
            <div className="member-container">
                <p className="mr-bot0">Game Designer</p>
                <p className="mr-top0">Thai Viet Y</p>
            </div>
            <div className="member-container">
                <p className="mr-bot0">Member</p>
                <p className="mr-top0">Nguyen Cao Minh Huyen, Lam Hong Phuc, Le Duc Luu, Do Hoan Gia Tri</p>
            </div>
        </div>
        <div className="about-project">
            <h2 >OVERVIEW</h2> <div/>
            <p className="mr-top0 project-script">With the aim of preserving and promoting the value of the Special National Monument, "Quang Tri Ancient Citadel," where the 81-day battle for Vietnam's independence took place, and also my hometown where I was born, I have manipulated my knowledge and passion to bring this heritage onto a VR digital platform. This place has deteriorated and suffered much damage over the years, so I am determined to use my skills to create a virtual experience that captures its essence.</p>
            <img src={pic1}></img>
            <h2 >TARGET</h2>
            <h2 >METHOD</h2>
            <p className="mr-top0  project-script">
            With the remarkable development of VR technology in late 2021, thanks to the emergence of "Metaverse," and the increasing demand from users for comprehensive experiences, I have chosen Virtual Reality technology, along with the Unreal Engine 5 game engine, for this project.<br/>
            The audience that I aim to target is those interested in promoting and preserving historical value, as well as utilizing visual education to teach history in Vietnam. Currently, there is a lack of interactive tools, and the teaching methods heavily rely on theory. Therefore, my goal is to provide an immersive and interactive experience that enhances the understanding of history in a more engaging way.
            </p>
            <div className="col-2">
                <div className="project-script">
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">VR TECHNOLOGY</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">GAME ENGINE</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">3D MODELING</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">PHOTOGRAMMETRY</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">PANORAMA</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">NERFs</h4>
                    </div>
                </div>
                <div className="project-script">
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">PCVR</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">UNREAL ENGINE 5</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">BLENDER - MAYA</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">3DF ZEPHYR</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">PANORAMA 360 PHOTO</h4>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <h4 className="mr-top0">NEUTRAL RADIANCE FIELDS</h4>
                    </div>
                </div>
            </div>
            <h2 >TECHNIQUE USED</h2>
            <h2 >DATABASE</h2>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">Graphic : Unreal Engine 5</p>
                <p className="mr-top0 mr-bot0 ">Code : C++ </p>
                <p className="mr-top0 mr-bot0 ">Moving Mechanism: Locomotion</p>
                <p className="mr-top0 mr-bot0 ">Export : PC application (.EXE)</p>
                <p className="mr-top0 mr-bot0 ">Optimal device: PCVR with airlink/cable connection - Eg: Oculus Quest 2, RiftS</p>
            </div>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">1. Monument measurement data. (72900㎡)</p>
                <p className="mr-top0 mr-bot0 ">2. Photo sequences of all detailed artifacts. (23 figures, ~ 3000 photos)</p>
                <p className="mr-top0 mr-bot0 ">3. Performance script</p>
                <p className="mr-top0 mr-bot0 ">4. Voiceover narration.(recording , editing)</p>
            </div>
        </div>
        <div id="slash" className="divider"></div>
        <div className="about-project">
            <h2>BUILDING ENVIRONMENT</h2>
            <div/>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">1. Create an overview map and generalize the dimensions through measurements with Google Maps and observations.</p>
                <p className="mr-top0 mr-bot0 ">2. Use Blender and Aaya to 3d model the overall terrain.</p>
                <p className="mr-top0 mr-bot0 ">3. Import 3D model to Unreal Engine 5 and programming the enviroment.</p>
                <p className="mr-top0 mr-bot0 ">4. Using 3D Asset (trees, grasses, bushes, materials, floors) from Quixel Bridge.</p>
                <p className="mr-top0 mr-bot0 ">5. Optimize lighting and graphics configuration.</p>
            </div>            
            <div className="col-2">
                <img src={pic2}></img>
                <img src={pic3}></img>
            </div>
            <h2 >MOVING MECHANISMS</h2>
            <div/>
            <p className="mr-top0 mr-bot0  project-script">Common VR motion mechanics like “Motion-tracked 6 points” or “Blink” have some limitations and don't feel real. Hence I used the “Locomotion” mechanism.</p>
            <img src={pic4}></img>
            <h2 >NERFs</h2> <div/>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">Methods for reconstructing objects from images, such as photogrammetry or 3D scans, can be costly and lack automatic capabilities. To overcome these challenges and save time, I have utilized Nvidia's open-source method called NERFs. This approach automates the reconstruction process, allowing for more efficient and streamlined results.</p>
                <p>NERF utilizes photo sequences of artifacts, similar to photogrammetry, but with notable improvements and a higher level of customizability. This technique enhances the reconstruction process by incorporating specific enhancements and allowing for greater flexibility in capturing and generating accurate 3D models from the artifact images.</p>
            </div>
            <div>
                <img src={pic5}></img>
                <img src={pic6}></img>
            </div>
            <h2 >PANORAMA</h2>
            <div/>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">In my software, I have integrated both 3D graphics and realistic panoramic photos to provide users with a truly immersive and multi-dimensional experience.</p>
                <p>To capture panoramic images, I keep the phone in a fixed position while taking multiple photos. These photos are then processed using the HDReye server, resulting in a 360-degree panorama file. This technique allows for a seamless and immersive viewing experience for users.</p>
            </div>
            <img src={pic7}></img>
            <h2>HISTORICAL COMMENTARY</h2> <div/>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">In collaboration with the monument council, I collected voiceovers from the guides and developed narrative scripts for each area.</p>
                <p>I transcribed the narration into text and utilized AI to read it in English. English subtitles have also been added to the software for a broader audience reach.</p>
                <p>I used a trigger box to activate the narration within specific areas of the software.</p>
            </div>
            <div className="grid-2-col">
                <img src={pic8}></img>
                <img src={pic9}></img>
                <img src={pic10}></img>
                <img src={pic11}></img>
            </div>
            <h2 >MINIMAP</h2> <div/>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">I added a minimap function to improve the user experience, helping them locate their coordinates and find their desired locations within the software.</p>
                <p>The minimap utilizes a 2D Ortho Camera that projects from a top-down perspective along the Z-axis and directly overwrites the materials. This simple mechanism helps conserve processing resources, resulting in optimized performance for the software.</p>
                <p>In the PC version, the minimap is displayed in the right corner of the user interface. In the VR version, users can toggle the minimap by pressing the X button on the VR controller.</p>
            </div>
            <img src={pic12}></img>
            <h2>VIRTUAL THURIFY</h2>
            <div/>
            <div className="project-script">
                <p className="mr-top0 mr-bot0 ">The Virtual Thurify mechanism provides a one-of-a-kind experience not found in other software. Users can physically interact by holding, grasping, dropping, and plugging incense sticks into specific positions, such as incense burners.</p>
                <p>When the virtual control handle touches an object, I retrieve the object's override value. Holding down the button allows the object's coordinate value to mimic the movement of the control handle, creating a realistic simulation of holding the object.</p>
            </div>
            <div className="col-2">
                <img src={pic15}></img>
                <ReactPlayer controls="true" width='max-content' url= "https://youtu.be/Zt0oPJ_XjYk"></ReactPlayer>
            </div>
            <h2>RESULT</h2> <div/>
            <div className="project-script">
                <span >Designed and built the software “Building Virtual Reality Model Of Special National Monument Quang Tri Ancient Citadel”</span><br/>
                <span >Field : System Software (SOFT), Education and tourism</span><br/>
                <span >Execution time : 08/2022 - 03/2023</span><br/>
                <span >Reconstruction size : 72900m2</span><br/>
                <span >Artifacts : 16/25 Objects</span><br/>
                <span >Size : 893mb</span><br/>
                <span >Platform : VR, PC</span>
            </div>
            <div className="col-2">
                <img src={pic16}/>
                <img src={pic17}/>
            </div>
        </div>
        <div className="demo">
            <h2>DEMO VIDEO - VR VERSION</h2>
            <ReactPlayer controls="true" url="https://youtu.be/FCNDSl10-8M" width="100%" id="demo-vid1"></ReactPlayer>
               
            <h2>DEMO VIDEO - PC VERSION</h2>
            <ReactPlayer controls="true" url="https://youtu.be/v0j4-kYdgGI" width="100%" id="demo-vid2"></ReactPlayer>
            
         
            <img src={pic18} />
        </div>
        <div className="experiment">
            <h2>EXPERIMENT</h2>
            <div className="grid-2-col">
                <div className="vertical-center">
                    <img src={pic32}></img>
                    <div className="col-2">
                        <img src={pic33}/>
                        <ReactPlayer controls="true" url="https://youtu.be/9x1XXkfuYwQ" height="425px"width="385px"></ReactPlayer>
                    </div>
                    <img src={pic34}/>                    
                    <img src={pic35}/>                    
                </div>
                <div className="vertical-center">
                    <img src={pic36}/>
                    <img src={pic37}/>
                    <ReactPlayer controls="true" style={{"margin-top" : "10px"}} url="https://youtu.be/mOWE6ORuXB8" height="885px"/>
                </div>
            </div>
        </div>
        <div className="practical-experince">
            <h2>PRACTICAL EXPERINCE</h2>
            <div className="grid-2-col">
                <div>
                    <span >The product has been tested by 75 users in 3 workshops and 2 surveys.
    These pie charts illustrates the percentage of users scoring by criterion : (1-10)</span>
                    <img src={pic38}/>
                </div>
                <div>
                    <div className="col-2">
                        <div id="dot"/>
                        <span>The survey participants belonged to many professions and ages such as students, teachers, journalists, the elderly,...</span>
                    </div>
                    <div className="col-2">
                        <div id="dot"/>
                        <span>It's clear that the scores are all above average, with no below average ratings. All criteria are well received.</span>
                    </div>
                    <p className="mr-bot0">Some user feedbacks on Google Form:</p>
                    <div className="grid-2-col">
                        <img src={pic39}/>
                        <div className="col-2">
                            <span>TRANSLATE: "After experiencing the Virtual tour of Quang Tri ancient citadel, I found it quite interesting to be able to experience it through VR, and the feeling was very new. But after the experience, I felt a bit dizzy. <br/>
Overall, everything was okay, and it would be even better if all areas and objects in the ancient citadel were complete.”</span>
                        </div>
                        <img src={pic40}/>
                        <div className="col-2">
                            <span>TRANSLATE: “The recent experience gave me a very authentic and smooth feeling, as I could move in the Virtual Reality space very quickly like gliding on the ground or like a character in a game. <span className="highlighted-span">My most impressive moment was when I stepped up to a rectangular lake and then jumped down into it, sank through the surface and landed on a grassy hill. The feeling at that time was so real that it made me startle and panic as if I had fallen from a height and then ran on the grass, climbed the hill...</span>I think the graphics are even more beautiful than the actual scene with very fresh colors, but I still prefer the dull scenery with faded colors over time in the ancient city to have more authentic, familiar, natural feelings. To perfect the project, the team can add other details such as stone steles with poems and especially the museum with important artifacts. With that, people can experience almost completely the feeling of traveling through virtual reality glasses. In the features, I rated 4/5 because I did not see any virtual reality narration voice. I believe that will be added soon. AND NGUYEN PHUONG IS ALSO VERY IMPRESSED WITH THE CREATIVE IDEA AND ABOVE ALL, THE ABILITY TO REPRODUCE AUTHENTIC SCENERY. It was a lucky moment, and I sincerely thank you for giving me the opportunity to experience your product.”</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="union-award">
            <h2>UNION AWARD</h2>
            <p>With this product, in 2023 me and my teammate participated in 2 national competitions.</p>
            <div className="grid-2-col ">
                <div className="award-container center-items">
                    <img src={pic41}/>
                    <p className="mr0">4th Prize at ViSEF 2023</p>
                </div>
                <div className="award-container center-items">
                    <img src={pic42}/>
                    <p className="mr0">1st Prize at SV-STARTUP 2023</p>
                </div>
                <div id="center-award" className="award-container center-items">
                    <img src={pic43}/>
                    <p className="mr0">Especially, in both of competitions, I wore a RMIT lanyard - a gift from my friend Bach gave me in<br/> my tour to RMIT SGS Campus. I use it as my motivation and as a good luck charm.<br/><br/>And lastly, thanks to Huyen, Phuc, Tri and Luu for being the best teammates ever.</p>
                </div>
            </div>
        </div>
        {/* <Footer/> */}
    </div>
)}