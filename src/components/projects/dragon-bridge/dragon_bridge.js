import Footer from "../../footer"
import ReactPlayer from "react-player/lazy"
import "./dragon_bridge.css"
import pic1 from "./recources/pic1.png"
import pic2 from "./recources/pic2.png"
import pic3 from "./recources/pic3.png"
import pic4 from "./recources/pic4.png"
import logo_white from "../../../resources/logo-white.svg"

export default function Dragon_bridge(){
    window.onload = () => {
        document.getElementById('loading').style.display = 'none'
        document.getElementById("navbar-links1").style.color = 'white'
        document.getElementById("navbar-links2").style.color = 'white'
        document.getElementById("navbar-links3").style.color = 'white'
        document.getElementById("navbar-links4").style.color = 'white'
        document.getElementById("logo-svg").src = logo_white
        document.getElementById("App").style.background = "black"
        // document.getElementById("navbar-input").style.borderColor = "#888888"
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
        const navbar = document.getElementById("navbar-container")
        navbar.style.borderColor = "#888888"
        document.getElementById('loading').style.display = 'none'

    }
    return (
        <div className="dragon-bridge">
            <div className='back-btn'>
                <div id="back-pointer"></div>
                <a href="/works">Back to Works</a>
            </div>
            <div className='project-title'>
                <h3 className="mr0">PROJECT</h3>
                <h1 className="mr0">THE DRAGON BRIDGE</h1>
            </div>
            <div className="expand-fullscreen">
                <div className="content-wrapper">
                    <img src={pic1}/>
                    <div className="col-2">
                        <p> <span className="bold-span">Da Nang </span>- The most beautiful city of Vietnam, known as one of the liveable cities and has the most<br/> beautiful and clean coastline in the world. It is associated with many iconic landmarks and bridges.<br/> Dragon Bridge is also among them.</p>
                    </div>
                    <img src={pic2}/>
                    <img src={pic3}/>
                    <div className="col-2">
                        <ReactPlayer controls="true" width="595px" url="https://www.youtube.com/watch?v=6NS0ojwoS-U"/>
                        <ReactPlayer controls="true" width="595px" url="https://www.youtube.com/watch?v=-9rjS1t-BdI"/>
                    </div>
                    <img src={pic4}/>
                </div>
                {/* <Footer/> */}
            </div>
        </div>
    )
}