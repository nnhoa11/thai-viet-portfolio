import "./automated-robot.css"
import ReactPlayer from "react-player/lazy"
import pic1 from "./recources/pic1.png"
import pic2 from "./recources/pic2.png"
import pic3 from "./recources/pic3.png"
import pic4 from "./recources/pic4.png"
import pic5 from "./recources/pic5.png"
import Footer from "../../footer"

export default function Automated_robot(){
    window.onload = () =>{
        document.getElementById('loading').style.display = 'none'
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
    }
    return(
        <div className="automated-robot">
            <div className='back-btn'>
            <div id="back-pointer"></div>
            <a href="/works">Back to Works</a>
            </div>
            <div className='project-title'>
                <h3 className="mr0">PROJECT</h3>
                <h1 className="mr0">Automated Robot Collects Samples For COVID-19 Testing (2021)</h1>
            </div>
            <div className="project-member-4 expand-fullscreen">
                <div className="member-container">
                    <p className="mr-bot0">3D Modeling</p>
                    <p className="mr-top0">Thai Viet Y</p>
                </div>
                <div className="member-container">
                    <p className="mr-bot0">Stimulating</p>
                    <p className="mr-top0">Thai Viet Y</p>
                </div>
                <div className="member-container">
                    <p className="mr-bot0">Mechanism</p>
                    <p className="mr-top0">Tran Quoc Hung</p>
                </div>
                <div className="member-container">
                    <p className="mr-bot0">Programming</p>
                    <p className="mr-top0">Thai Viet Y & Tran Quoc Hung</p>
                </div>
            </div>
            <div className="overview">
                <h2>OVERVIEW</h2>
                <div className="grid-2-col">
                    <div>
                        <p >According to the statistics from the WHO's General Statistics Office, as of February 8th, 2022, there have been 396,558,014 confirmed cases of COVID-19 globally, with 5,745,032 deaths reported to the WHO. However, the pandemic has become more complex with the emergence of new dangerous variants such as Delta and Omicron.</p>
                        <p >In response to the pandemic, many measures to prevent and control the disease have been implemented, including the collection of specimens to test for the SARS-CoV-2 virus. Currently, specimen collection is mainly carried out by humans, leading to many limitations such as: putting pressure on healthcare workers, high risk of cross-infection, dependence on the expertise and psychology of the sample collector, which can cause discomfort to the person being sampled and may affect the quality of the collected sample. The question is how to collect specimens that can overcome these limitations. Countries like China, Singapore, and South Korea have conducted research and development of various robot sampling models, but we have not yet had access to them.</p>
                        <p >The “Automated Robot Collects Samples For COVID-19 Testing” which utilizes machine learning and AI technologies has been developed.</p>
                    </div>
                    <img src={pic1}/>
                </div>
            </div>
            <div className="grid-2-col">
                <div className="target">
                    <h2>TARGET</h2>
                    <p >Research is being conducted to create a robot capable of autonomously collecting specimens for infectious diseases, supporting healthcare workers in epidemic prevention with high accuracy and low cost. This will reduce discomfort for the person being sampled. Additionally, the system will include data storage for the individuals sampled and testing procedures for the collected specimens. It is especially important to ensure there is no risk of cross-infection.</p>
                </div>
                <div className="method">
                    <h2>METHOD</h2>
                    <div className="col-2">
                        <div>
                            <span >MECHANICAL</span><br/>
                            <span >AI</span><br/>
                            <span >3D MODELING</span><br/>
                            <span >GEAR MOVEMENT</span><br/>
                            <span >ARDUINO</span>
                        </div>
                        <div>
                            <div className="col-2">
                                <div id="dot"/>
                                <span >3D DESIGN</span>
                            </div>
                            <div className="col-2">
                                <div id="dot"/>
                                <span >DEEP LEARNING, MACHINE LEARNING</span>
                            </div>
                            <div className="col-2">
                                <div id="dot"/>
                                <span >FORCE SENSOR, STEPPER, MOTOR, SERVO</span>
                            </div>
                            <br/>
                            <div className="col-2">
                                <div id="dot"/>
                                <span >MEGE2560, NODEMCU ESP8266</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="database project-script">
                    <h2>DATABASE</h2>
                    <p className=" mr-bot0">1. Machine learning data</p>
                    <p className=" mr-top0">2. Stimulate test and eliminate bad results optimally</p>
                </div>
                <div className="progress">
                    <h2>PROGRESS</h2>
                    <p className=" mr-bot0">1. Researching, find the optimal solution.</p>
                    <p className=" mr-bot0 mr-top0 ">2. Detailed plan.</p>
                    <p className=" mr-bot0 mr-top0">3. 3D model of the stepper system on the computer.</p>
                    <p className=" mr-bot0 mr-top0">4. Calculate the optimal gear system for motion.</p>
                    <p className=" mr-bot0 mr-top0">5. 3D printing detail.</p>
                    <p className=" mr-bot0 mr-top0">6. Optimizing system and wiring.</p>
                    <p className=" mr-bot0 mr-top0">7. Loading machine learning and testing data.</p>
                </div>
            </div> 
            <div id="slash" className="divider"></div>
            <div className="reseach-manufacture">
                <h2>RESEARCH AND MANUFACTURE ROBOT PARTS</h2>
                <img src={pic2}/>
            </div>
            <div className="working-principle">
                <h2>WORKING PRINCIPLE</h2>
                <img src={pic3}/>
            </div>
            <div className="demo-vid">
                <h2>DEMO VIDEO</h2>
                <ReactPlayer controls="true" url="https://www.youtube.com/watch?v=ed1h9t_nMZw" width="100%" height="1360px"/>
            </div>
            <div className="result">
                <h2>RESULT</h2>
                <div className="grid-2-col">
                    <div>
                        <span >Designed and built “Automated Robot Collects Samples For COVID-19 Testing”</span><br/>
                        <span >Time : 09/2021 - 03/2022</span><br/><br/>
                        <div className="col-2">
                            <div id="dot"/>
                            <span >The automatic sampling process takes about 30-40 seconds per person.</span>
                        </div>
                        <div className="col-2">
                            <div id="dot"/>
                            <span >The cost of the robot product is around 4.5 million VND, making it suitable for widespread deployment.</span>
                        </div>
                        <div className="col-2">
                            <div id="dot"/>
                            <span >The robot has an automatic disinfection system to avoid cross-infection risks when collecting specimens like human collectors.</span>
                        </div>
                        <div className="col-2">
                            <div id="dot"/>
                            <span >Minimal discomfort for the person being sampled: the group conducted a test with 30 people who had been previously sampled by healthcare workers, and the results showed that all 30 individuals found the robot sampling process more comfortable than the human one.</span>
                        </div>
                        <div className="col-2">
                            <div id="dot"/>
                            <span >The quality of the samples collected by the robot ensures enough quality to perform accurate testing.</span>
                        </div>
 
                    </div>
                    <img src={pic4}/>
                </div>
            </div>
            <div className="union-award">
                <h2 className="mr-bot0">UNION AWARD</h2>
                <span >With this product, in 2023 me and my teammate participated in the ViSEF and won the 3rd prize.</span>
                <img src={pic5}/>
            </div>
            {/* <Footer/> */}
        </div>
    )
}