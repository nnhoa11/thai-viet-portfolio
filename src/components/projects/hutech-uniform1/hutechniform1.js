import "./hutechniform1.css"
import pic1 from "./recources/pic1.png"
import pic2 from "./recources/pic2.png"
import pic3 from "./recources/pic3.png"
import pic4 from "./recources/pic4.png"
import pic5 from "./recources/pic5.png"
import pic6 from "./recources/pic6.png"
import pic7 from "./recources/pic7.png"
import pic8 from "./recources/pic8.png"
import pic9 from "./recources/pic9.png"
import Footer from "../../footer"
export default function Hutech_uniform1(){
    window.onload = () =>{
    
        document.getElementById('loading').style.display = 'none'
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
    }
    return(
        <div className="hutech-uniform1">
            <div className='back-btn'>
                <div id="back-pointer"></div>
             <a href="/works">Back to Works</a>
            </div>
            <div className='project-title'>
                <h3>PROJECT</h3>
                <h1 className="mr0">HUTECH Uniform Design Competition 2021</h1>
            </div>
            <div className="project-member-6 expand-fullscreen">
                <div className="member-container">
                    <h4 className="mr-bot0">Director</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Designer</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Script</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Concept</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Photographer</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
                <div className="member-container">
                    <h4 className="mr-bot0">Compositing</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
            </div>
            <h3>ALBUM TOPIC</h3>
            <div className="grid-2-col">
                <div className="flex-2-row">
                    <div>
                        <p>Entering the new environment was still unfamiliar, but after half a year of studying and playing together, every member of the class came together as a cohesive group. Although our class wasn't large in size, it made it easier for everyone to integrate and get along with one another.</p>
                        <p>This was an opportunity for us to have fun and spend a whole day together, building more beautiful memories during our high school years. As the theme of our album  "What piece do you choose for your own dream?," we always try to make our youth under Le Quy Don High school for the Gifted the most memorable time of our lives. The three years of high school were really short, and we value each memory, constantly building on them.</p>  
                        <p>Gradually, each person has their own separate path in life, and no two are alike. However, memories will be what connects our 24 hearts together, helping us remember this unforgettable youth.</p>
                    </div>
                    <div className="subject">
                        <h4 className="mr-bot0">Vietnamese traditional culture mixed with modern information technology.</h4>
                        <span>Color : Ocean Blue and Yellow Orange</span> <br/>
                        <span>Symbol : Chim Lac</span>
                        <div className="col-2">
                            <img src={pic2}/>
                            <img src={pic3}/>
                        </div>
                        <h4>Pantone</h4>
                        <div className="col-2">
                            <img src={pic4}/>
                            <img src={pic5}/>
                        </div>
                    </div>
                    
                </div>
                <img src={pic1}/>
            </div>
            <div className="union-award">
                <h3>DESIGN</h3>
                <p>The original design featured yellow lines on the collar and under the shirt. But in the end I<br/> decided to remove them to keep the simplicity and highlight the face of the owner.</p>
                <img src={pic6}/>
            </div>
            <div className="grey-background expand-fullscreen">
                <img src={pic7}/>
            </div>
            <div className="result">
                <h2>RESULT</h2>
                <img src={pic8}/>
                <img src={pic9}/>
            </div>
            {/* <Footer /> */}
        </div>
    )
}