import "./olympus.css"
import Footer from "../../footer"
import pic1 from "./recources/pic1.png"
import pic2 from "./recources/pic2.png"
import pic3 from "./recources/pic3.png"
import pic4 from "./recources/pic4.png"
import pic5 from "./recources/pic5.png"
import pic6 from "./recources/pic6.png"
import pic7 from "./recources/pic7.png"
import pic8 from "./recources/pic8.png"
import pic9 from "./recources/pic9.png"

export default function Olympus(){
    window.onload = ()=>{
        document.getElementById('fl3').style.background = "#2A2A2A"
        document.getElementById('fl3').style.color = "white"
        document.getElementById('loading').style.display = 'none'

    }
    return (
        <div className="olympus">
            <div className='back-btn'>
                <div id="back-pointer"></div>
                <a href="/works">Back to Works</a>
            </div>
            <div className='project-title'>
                <h3 className="mr0">PROJECT</h3>
                <h1 className="mr0">“ROAD TO OLYMPUS” Brand Indentity</h1>
            </div>
            <div className="project-member-6 expand-fullscreen">
                <div id="only" className="member-container">
                    <h4 className="mr-bot0">Designer</h4>
                    <h4 className="mr-top0">Thai Viet Y</h4>
                </div>
            </div>
            <h2>OVERVIEW</h2>
            <div className="grid-2-col">
                <div>
                    <p className="mr-top0">Road to Olympus” is a school-level competition organized based on the concept of the famous TV show 'Road to Olympia' with the aim of providing a knowledge playground for students at Le Quy Don High School for the Gifted as well as selecting outstanding individuals to register for the 'Road to Olympia' competition.</p>
                    <p className="mr-top0">As of now, the program has organized 2 seasons and I have contributed in the role of a designer for both seasons.</p>
                </div>
                <img src={pic1}/>
                <h2>DESIGN</h2>
                <div/>
                <div>
                    <p className="mr-top0 mr-bot0">With the logo, I aim to bring a modern and minimalist look so that the product can be used flexibly across different platforms.</p>
                    <p className="mr-top0 mr-bot0">The symbol of 3 stacked books forming a mountain from low to high represents the comparison of knowledge to a mountain, and how we must conquer it step by step like mountain climbers.</p>
                    <p className="mr-top0 mr-bot0">The yellow circle resembling the sun represents the radiance of knowledge.</p>
                </div>
                <img src={pic2}/>
            </div>
            <h2 >PRODUCT</h2>
            <div className="grid-2-col-2-row">
                <div>
                    <img src={pic3}/>
                    <h3>Certificate</h3>
                </div>
                <div>
                    <img src={pic4}/>
                    <h3>Fanpage avatar</h3>
                </div>
                <div>
                    <img src={pic5}/>
                    <h3>Backdrop</h3>
                </div>
                <div>
                    <img src={pic6}/>
                    <h3>Laurel Wreath</h3>
                </div>
            </div>
            <h2>CONTRIBUTION</h2>
            <div className="grid-2-col-2-row">
                <img id="center-award" src={pic7}/>
                <img src={pic8}/>
                <img src={pic9}/>
            </div>
            {/* <Footer></Footer> */}
        </div>
    )
}